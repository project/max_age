<?php

/**
 * @file
 * Definition of max_age_cache_time.
 */

/**
 * Add max age to simple caching of query results for Views displays.
 */
class max_age_cache_time extends views_plugin_cache_time {

  function option_definition() {
    $options = parent::option_definition();
    $options['page_lifespan'] = array('default' => 3600);
    $options['page_lifespan_custom'] = array('default' => 0);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = array(60, 300, 1800, 3600, 21600, 518400);
    $options = drupal_map_assoc($options, 'format_interval');
    $options = array(-1 => t('Never cache')) + $options + array('custom' => t('Custom'));

    $form['page_lifespan'] = array(
      '#type' => 'select',
      '#title' => t('Cached page'),
      '#description' => t('Length of time in seconds rendered page output should be cached by a reverse proxy. If there are several views on a page, the lowest value will be used.'),
      '#options' => $options,
      '#default_value' => $this->options['page_lifespan'],
    );
    $form['page_lifespan_custom'] = array(
      '#type' => 'textfield',
      '#title' => t('Seconds'),
      '#size' => '25',
      '#maxlength' => '30',
      '#description' => t('Length of time in seconds rendered page output should be cached by a reverse proxy. If there are several views on a page, the lowest value will be used.'),
      '#default_value' => $this->options['page_lifespan_custom'],
      '#process' => array('form_process_select', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-cache-options-page-lifespan' => array('custom'),
      ),
    );
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
    $custom_fields = array('page_lifespan');
    foreach ($custom_fields as $field) {
      if ($form_state['values']['cache_options'][$field] == 'custom' && !is_numeric($form_state['values']['cache_options'][$field . '_custom'])) {
        form_error($form[$field . '_custom'], t('Custom time values must be numeric.'));
      }
    }
  }

  function summary_title() {
    $title = parent::summary_title();
    $page_lifespan = $this->get_lifespan('page');
    return $title . '/' . format_interval($page_lifespan, 1);
  }

  function cache_expire($type) {
    $lifespan = $this->get_lifespan($type);
    // This method doesn't actually get called by views with a $type of 'page'
    // so we piggyback on the other types.
    // @todo: could probably avoid doing this twice (results and output)
    if (drupal_page_is_cacheable() && ($page_lifespan = $this->get_lifespan('page'))) {
      _max_age($page_lifespan);
    }
    parent::cache_expire($type);
  }

  function cache_set_expire($type) {
    $lifespan = $this->get_lifespan($type);
    // This method doesn't actually get called by views with a $type of 'page'
    // so we piggyback on the other types.
    // @todo: could probably avoid doing this twice (results and output)
    if (drupal_page_is_cacheable() && ($page_lifespan = $this->get_lifespan('page'))) {
      _max_age($page_lifespan);
    }
    parent::cache_set_expire($type);
  }

}

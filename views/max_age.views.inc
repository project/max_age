<?php

/**
 * @file
 * Views integration for max_age.
 */

/**
 * Implements hook_views_plugins().
 */
function max_age_views_plugins() {
  $plugins = array(
    'module' => 'views', // This just tells our themes are elsewhere.
    'cache' => array(
      'max_age_cache_time' => array(
        'title' => t('Max Age'),
        'help' => t('Time based caching for views output. Sets a max age header.'),
        'handler' => 'max_age_cache_time',
        'uses options' => TRUE,
      ),
    ),
  );
  if (module_exists('views_content_cache')) {
    $plugins['cache']['max_age_content_cache'] = array(
      'parent' => 'views_content_cache',
      'title' => t('Content-based (max age)'),
      'help' => t('Advanced content based caching options for views. Sets a max age header.'),
      'handler' => 'max_age_content_cache',
      'path' => drupal_get_path('module', 'max_age') . '/views',
      'uses options' => TRUE,
    );
  }

  return $plugins;
}

<?php
/**
 * @file
 * Definition of max_age_content_cache.
 */

/**
 * Add max age to Views Content Cache plugin.
 */
class max_age_content_cache extends views_content_cache_plugin_cache {

  function option_definition() {
    $options = parent::option_definition();
    $options['page_lifespan'] = array('default' => 21600);

    return $options;
  }

  function option_defaults(&$options) {
    parent::option_defaults($options);
    $options['page_lifespan'] = 21600;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array(60, 300, 1800, 3600, 21600, 518400);
    $options = drupal_map_assoc($options, 'format_interval');
    $max_options = array(-1 => t('None')) + $options + array(-2 => t('Never cache'));

    $form['page_lifespan'] = array(
      '#type' => 'select',
      '#title' => t('Page output - Max age cache header'),
      '#description' => t("Set the max age header. If another such header is set on a given page, the lower value wins."),
      '#options' => $max_options,
      '#default_value' => $this->options['page_lifespan'],
    );
  }

  function summary_title() {
    return "Content cache (max age)";
  }

  /**
   * Return the expiry time for this cache plugin.
   *
   * This should be the last time that the content has changed, altered to allow
   * for the the min/max lifetimes.
   */
  function cache_expire($type) {
    if (drupal_page_is_cacheable() && ($lifespan = $this->options['page_lifespan'])) {
      _max_age($lifespan);
    }
    return parent::cache_expire($type);
  }

  function cache_set_expire($type) {
    if (drupal_page_is_cacheable() && ($lifespan = $this->options['page_lifespan'])) {
      _max_age($lifespan);
    }
    return parent::cache_set_expire($type);
  }

}

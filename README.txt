-- SUMMARY --

A very small module that allows for the page cache maximum age to be customised
per node type in addition to the site-wide default.

Also integrates with Views so that displays using time-based or content-based
caching can emit a cache-control header based on the shortest max-age of any
nodes included in the results. Look for the "max age" options in the cache
settings for your views displays.

-- ALTERNATIVES --

If you are looking for a module with more options, try APE:

https://www.drupal.org/project/ape

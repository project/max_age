<?php

/**
 * @file
 * Allows finer grained control over page cache maximum age.
 */

/**
 * Implements hook_form_alter().
 */
function max_age_form_system_performance_settings_alter(&$form, &$form_state) {
  $period = array(-1 => '<' . t('None') . '>') + drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400), 'format_interval');
  $types = node_type_get_names();

  $form['page_cache']['max_age'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page cache maximum age'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  foreach (array_keys($types) as $type) {
    $form['page_cache']['max_age']["max_age_node_$type"] = array(
      '#type' => 'select',
      '#title' => t('Page cache maximum age for node type: @type', array('@type' => $type)),
      // We use -1 as a setting for "none" as the default. That means that we
      // can override the default using a view even on a node/n page.
      '#default_value' => variable_get("max_age_node_$type", -1),
      '#options' => $period,
    );
  }
}

/**
 * Implements hook_node_view().
 */
function max_age_node_view($node, $view_type) {
  // Use the global page cache maximum age variable if none is set.
  $max_age = variable_get("max_age_node_$node->type", variable_get('page_cache_maximum_age', 0));
  // Manipulating $conf here won't work since the header is sent just after
  // hook_boot(), however we can set the header ourselves.
  if (drupal_page_is_cacheable()) {
    _max_age($max_age);
  }
}

/**
 * If there are multiple content-types on a page, use the smallest max-age.
 */
function _max_age($age = NULL) {
  static $max_age = NULL;

  if (isset($age) && $age >= 0 && (!isset($max_age) || $age < $max_age)) {
    $max_age = $age;
  }

  return $max_age;
}

/**
 * Implements hook_exit().
 */
function max_age_exit() {
  $max_age = _max_age();
  if (!empty($max_age)) {
    drupal_add_http_header('Cache-Control', 'public, max-age=' . $max_age);
  }
}

/**
 * Implements hook_views_api().
 */
function max_age_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'max_age') . '/views',
  );
}
